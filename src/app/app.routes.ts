
import { ApplicationFormListComponent } from './application-form-list/application-form-list.component';
import { ApplicationFormDetailsComponent } from './application-form-details/application-form-details.component';
import { LoginComponent } from './login/login.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AlbumsComponent } from './albums/albums.component';
import { AlbumDetailsComponent } from './albums/album-details/album-details.component';
import { AnimationAnimateMetadata } from '@angular/core';
import { Routes } from '@angular/router';

export const rootRouterConfig: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },

    { path: 'application-form-list', component: ApplicationFormListComponent },

    { path: 'albums', component: AlbumsComponent },
    { path: 'album', component: AlbumDetailsComponent },
    
    { path: 'notices', component: NotificationsComponent },

    { path: 'application-form-details/:email', component: ApplicationFormDetailsComponent },

    { path: 'login', component: LoginComponent },
];
