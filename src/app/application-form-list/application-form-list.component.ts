import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params, RouterModule } from '@angular/router';
import { AdmissionService } from './../app.service';
import { Admission } from './../app.model';
// import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-application-form-list',
  templateUrl: './application-form-list.component.html',
  styleUrls: ['./application-form-list.component.scss'],
})
export class ApplicationFormListComponent implements OnInit {

  public admissions: any; admissionId = []; totalReg;
  public filteredAdmissions: any;
  public tempAdmissionModel: any;
  page;
  public searchText;

  constructor(private router: Router, private _admissionService: AdmissionService, private cdr: ChangeDetectorRef) {

  }

  ngOnInit() {

    this.callGetAllAdmissionsAPI();
  }

  public callGetAllAdmissionsAPI() {

    this._admissionService.getAllAdmissions()
      .subscribe(response => {
        this.admissions = response;
        this.filteredAdmissions = this.admissions;
        this.tempAdmissionModel = this.admissions;
        this.totalReg = this.filteredAdmissions.length;
        // this.cdr.detectChanges();

      }, err => {
      });
  }


  getAdmissionExcel() {

    this._admissionService.makeFileRequest().then(
      data => {

        this.getExcel(data);
      },
      err => {
        alert("error");
      }
    );
  }

  getExcel(fileContents) {

    var linkElement = document.createElement('a');

    var url = window.URL.createObjectURL(fileContents);
    linkElement.setAttribute("href", url);
    linkElement.setAttribute("download", "StudentAdmissionList.xlsx");

    document.body.appendChild(linkElement);
    linkElement.click();
    document.body.removeChild(linkElement);
  }


  filterAdmissions(event) {
    this.searchText = event;
    console.log(this.searchText);
    if (this.searchText === '') {
      this.filteredAdmissions = this.admissions;
    }
    else {
      // this.filteredAdmissions = this.admissions.filter(data => data.firstName.toLowerCase().includes(this.searchText.toLowerCase()));

      this.filteredAdmissions = this.tempAdmissionModel.filter(
        function (d) {
          if (d.firstName == undefined || d.firstName == null || d.status == undefined || d.status == null) {

          }

          else {
            return d.firstName.toLowerCase().includes(this.searchText.toLowerCase()) ||
              d.email.toLowerCase().includes(this.searchText.toLowerCase()) ||
              d.status.toLowerCase().includes(this.searchText.toLowerCase())
          }
        }, this);
    }
  }

  deleteStudent(id, fname, lname) {

    if (confirm("Are sure you want to delete " + fname + " " + lname + "'s " + "record?")) {

      this._admissionService.deleteStudent(id).subscribe(
        data => {
          alert("Record deleted successfully");
          this.callGetAllAdmissionsAPI();
        }
      );
    }
  }

  goToDetailsPage() {
    this.router.navigate(['/application-form-details']);
  }

}
