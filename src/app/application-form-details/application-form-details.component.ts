import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, RouterModule } from '@angular/router';
// import {AccordionModule} from "ng2-accordion";
import { AdmissionService } from './../app.service';
import { Admission } from './../app.model';
// import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-application-form-details',
  templateUrl: './application-form-details.component.html',
  styleUrls: ['./application-form-details.component.scss']
})
export class ApplicationFormDetailsComponent implements OnInit {

  public email;
  public dateformat;
  public ddDateformat;
  public familyIncome; casteCode; seatNumber;
  public statusSpin = false; mumbaiUniversity = false;
  public statusnote;
  public admission = new Admission();

  constructor(private router: Router,
    private route: ActivatedRoute,
    // private localStorage: LocalStorageService,
    private _admissionService: AdmissionService) {

    this.route.params.forEach((params: Params) => {
      this.email = params['email'];
    });

    this.callGetAdmissionByEmailApi();
  }


  callGetAdmissionByEmailApi() {

    this._admissionService.getAdmissionByEmail(this.email)
      .subscribe(response => {
        this.admission = response;

        this.dateformat = this._admissionService.getHumanReadableDate(this.admission.dob.toString());

        this.ddDateformat = this._admissionService.getHumanReadableDate(this.admission.ddDate.toString());

        this.familyIncome = this._admissionService.getFamilyIncome(this.admission.familyIncomeBlock);

        this.statusnote = this.admission.statusNote;

        this.isMumbaiUniversity();


      }, err => {
        console.log("error", err);
      });
  }

  isMumbaiUniversity() {
    
    if ((this.admission.universityName !== null) && (this.admission.universityName.search(/mumbai/i) >= 0)) {

      this.mumbaiUniversity = true;
    }
  }

  public logout() {

    localStorage.clearAll();
    this.router.navigate(['/login']);
  }

  goToListPage() {
    this.router.navigate(['/application-form-list']);
  }


  callRejectAdmissionAPI() {

    var x;
    if (confirm("Are you sure you want to Reject this Admission?") == true) {
      let token = localStorage.getItem('access_token');

      var status = 'rejected';
      var statusnote = this.statusnote;

      this._admissionService.rejectAdmission(this.admission.userId, status, statusnote, token).subscribe(

        response => {
          window.scroll(0, 0);
          this.callGetAdmissionByEmailApi();
        });

    }

    else {
      x = "You pressed Cancel!";
    }

  }


  callApproveAdmissionAPI() {

    var x;

    if (confirm("Are you sure you want to Approve this Admission?") == true) {

      let token = localStorage.getItem('access_token');

      var status = 'approved';
      var statusnote = this.statusnote;

      this._admissionService.approveAdmission(this.admission.userId, status, statusnote, token).subscribe(

        response => {

          window.scroll(0, 0);
          this.callGetAdmissionByEmailApi();
        });

    }

    else {
      x = "You pressed Cancel!";
    }

  }

  markIncomplete() {

    var x;

    if (confirm("Are you sure you want to mark incomplete this Admission?") == true) {

      let token = localStorage.getItem('access_token');

      var status = 'incomplete';
      var statusnote = this.statusnote;

      this._admissionService.markIncompleteAdmission(this.admission.userId, status, statusnote, token).subscribe(

        response => {

          window.scroll(0, 0);
          this.callGetAdmissionByEmailApi();
        });

    }

    else {
      x = "You pressed Cancel!";
    }
  }

  ngOnInit() {
  }

}
