import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, RouterModule } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  loggedIn = false;

  constructor(private router: Router) {

    if (localStorage.getItem('access_token') !== null) {

      this.loggedIn = true;
    }
  }

  ngOnInit() {
  }

  public logout() {

    localStorage.clear();
    this.loggedIn = false;
    this.router.navigate(['/login']);
  }
}
