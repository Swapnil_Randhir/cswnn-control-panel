import { Component, OnInit } from '@angular/core';
import { AdmissionService } from './../app.service';
import { Router } from '@angular/router';
// import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public spin = false;
  public invalidCreds = false;

  constructor(private router: Router, private _admissionService: AdmissionService) {


  }

  ngOnInit() {
  }


  public login(username, password) {

    if (username !== 'admin@cswnn' && password !== 'jdUGE9$B#K') {

      this.invalidCreds = true;
      this.spin = false;
    }

    else {
      this.spin = true;
      this._admissionService.Login(username, password).subscribe(response => {

        localStorage.setItem('access_token', response.access_token);
        console.log("token: " + response.access_token);
        this.router.navigate(['/application-form-list']);
      }
        , err => {
          alert("Invalid Credentials");
          this.invalidCreds = true;
          this.spin = false;
        });
    }
  }

  public loginCredsChanged() {
    this.invalidCreds = false;
  }

}
