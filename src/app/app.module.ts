import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { RouterModule, RouterState } from '@angular/router';
import { rootRouterConfig } from './app.routes';

import {AccordionModule} from "ng2-accordion";

import { AppComponent } from './app.component';
import { AdmissionService } from './app.service';

import { ApplicationFormListComponent } from './application-form-list/application-form-list.component';
import { ApplicationFormDetailsComponent } from './application-form-details/application-form-details.component';
import { Ng2PaginationModule } from 'ng2-pagination';
import { LoginComponent } from './login/login.component';
// import { LocalStorageModule } from 'angular-2-local-storage';
import { RouteReuseStrategy } from '@angular/router';
import { CustomReuseStrategy } from './roterReuse.component';
import { AlbumsComponent } from './albums/albums.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AlbumDetailsComponent } from './albums/album-details/album-details.component';
import { ImageCropperModule } from 'ng2-img-cropper';

@NgModule({
  declarations: [
    AppComponent,
    ApplicationFormListComponent,
    ApplicationFormDetailsComponent,
    LoginComponent,
    AlbumsComponent,
    NotificationsComponent,
    AlbumDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AccordionModule,
    Ng2PaginationModule,
    ImageCropperModule,
    RouterModule.forRoot(rootRouterConfig, )
  ],
  providers: [AdmissionService, { provide: RouteReuseStrategy, useClass: CustomReuseStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
