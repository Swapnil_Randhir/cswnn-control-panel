import { RouteReuseStrategy, DetachedRouteHandle, ActivatedRouteSnapshot } from '@angular/router';

export class CustomReuseStrategy implements RouteReuseStrategy {

    routesToCache: string[] = ["application-form-list"];
    storedRouteHandles = new Map<string, DetachedRouteHandle>();

    shouldDetach(route: ActivatedRouteSnapshot): boolean {

        /** Determines if this route (and its subtree) should be detached to be reused later */
        return this.routesToCache.indexOf(route.routeConfig.path) > -1;
    }


    store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {

        /* Stores the detached route.
        Storing a `null` value should erase the previously stored value.*/

        this.storedRouteHandles.set(route.routeConfig.path, handle);
    }

    shouldAttach(route: ActivatedRouteSnapshot): boolean {
        /** Determines if this route (and its subtree) should be reattached */
        return this.storedRouteHandles.has(route.routeConfig.path);
    }

    retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
        /** Retrieves the previously stored route */
        return this.storedRouteHandles.get(route.routeConfig.path);
    }

    shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
        /** Determines if a route should be reused */
        return future.routeConfig === curr.routeConfig;
    }
}