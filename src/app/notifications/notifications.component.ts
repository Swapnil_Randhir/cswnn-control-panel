import { Component, OnInit } from '@angular/core';
import { AdmissionService } from './../app.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  public notices = []; photoResp; url; tempNotice; title; count = 0;

  public noticeTemplateArray = Array<NoticeTemplate>();

  showNotices = true; showAddNotice = false; selectedPhoto;

  constructor(private _admissionService: AdmissionService) { }

  ngOnInit() {

    this.getAllNotices();
  }

  getAllNotices() {

    this._admissionService.getAllNotices().subscribe(

      data => {

        this.notices = data;
        this.notices = this.notices.reverse();
      });
  }

  showAddNoticeSection() {

    this.url = "";
    this.title = "";
    this.showNotices = false;
    this.showAddNotice = true;

    this.noticeTemplateArray.push(new NoticeTemplate("", ""));
  }

  addNoticeTemplate() {

    this.noticeTemplateArray.push(new NoticeTemplate("", ""));
    ++this.count;
  }

  removeNoticeTemplate(template) {

    this.noticeTemplateArray.splice(this.noticeTemplateArray.indexOf(template), 1);
    --this.count;
  }

  close() {

    this.showNotices = true;
    this.showAddNotice = false;
    this.getAllNotices();
    this.noticeTemplateArray = [];
  }

  addNotice(notice) {

    const noticeObj = {

      title1: notice.title1,
      title2: notice.title2,
      title3: notice.title3,
      title4: notice.title4,
      title5: notice.title5,

      imageUrl1: notice.imageUrl1,
      imageUrl2: notice.imageUrl2,
      imageUrl3: notice.imageUrl3,
      imageUrl4: notice.imageUrl4,
      imageUrl5: notice.imageUrl5,

    }

    this._admissionService.postNotice(noticeObj).subscribe(

      data => {

        this.close();
        alert("Notice Added Successfully");
      }
    );
  }

  updateNotice(notice) {

    this.callUpdateNoticeApi(notice);
  }

  callUpdateNoticeApi(updateObj) {

    this._admissionService.updateNotice(updateObj).subscribe(

      data => {

        this.close();
        alert("Notice Updated Successfully");
      }
    );
  }

  deleteNotice(notice) {

    if (confirm('Confirm Delete')) {

      this._admissionService.deleteNotice(notice).subscribe(
        data => {

          this.getAllNotices();
          alert("Notice Deleted Successfully");
        }
      );
    }
  }

  publishNotice(notice) {

    notice.isPublished = true;

    this.callUpdateNoticeApi(notice);
  }

  unPublishNotice(notice) {

    notice.isPublished = false;

    this.callUpdateNoticeApi(notice);
  }

  openUpdateModal(notice) {
    
    this.tempNotice = notice;
  }

  selectPhoto(event) {

    let eventObj = event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    this.selectedPhoto = files[0];

    if (this.selectedPhoto.size >= 3 * 1024 * 1024) {

      alert('Please select image of size less than 3 MB');
    }
    else {
      this.uploadPhoto();
    }

  }

  uploadPhoto() {

    this._admissionService.uploadPhoto(this.selectedPhoto).subscribe(
      data => {

        this.photoResp = data;
        this.url = this.photoResp._body;

        this.noticeTemplateArray[this.count].imageUrl = this.url;
      }
    );
  }
}

export class NoticeTemplate {

  imageUrl: string;
  title: string;

  constructor(imageUrl: string, title: string) {

    this.imageUrl = imageUrl;
    this.title = title;
  }
}