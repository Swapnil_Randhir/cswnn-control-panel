import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
// import { LocalStorageService } from 'angular-2-local-storage';
import { Observable } from 'rxjs/Rx';
// import { Student } from './form.model';


@Injectable()

export class AdmissionService {

    public apiUrl: string;
    private tokenEndpoint: string;

    constructor(private http: Http) {
        this.apiUrl = "http://cswnnadmissionapi.azurewebsites.net";
        this.tokenEndpoint = "http://cswnnadmissionsts.azurewebsites.net" + '/connect/token';
    }

    casteMappingArray = [

        { caste: 'General', code: 'A' },
        { caste: 'Roman Catholic', code: 'B' },
        { caste: 'Other Christian', code: 'C' },
        { caste: 'OBC', code: 'F' },
        { caste: 'SC', code: 'D' },
        { caste: 'ST', code: 'E' },
        { caste: 'SBC', code: 'G' },
        { caste: 'NT', code: 'H' },
        { caste: 'DT', code: 'I' }
    ]

    Login(username, password) {

        let body = 'username=' + username + '&password=' + password + '&client_id=ro.client&client_secret=secret&scope=api1&grant_type=password';
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(this.tokenEndpoint, body, { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getAllAdmissions() {

        let accessToken = localStorage.getItem('access_token')
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + accessToken);
        return this.http.get(this.apiUrl + "/api/admissions/GetAdmissions", { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getAllNotices() {

        let accessToken = localStorage.getItem('access_token');
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + accessToken);
        headers.append('Content-Type', 'application/json');

        return this.http.get(this.apiUrl + "/api/Notices/GetNotices", { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getAllAlbums() {

        let accessToken = localStorage.getItem('access_token');
        let headers = new Headers();

        headers.append('Authorization', 'Bearer ' + accessToken);
        headers.append('Content-Type', 'application/json');

        return this.http.get(this.apiUrl + "/api/albums/GetAlbums", { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getAlbum(id) {

        let accessToken = localStorage.getItem('access_token');
        let headers = new Headers();

        headers.append('Authorization', 'Bearer ' + accessToken);
        headers.append('Content-Type', 'application/json');

        return this.http.get(this.apiUrl + "/api/albums/GetAlbum/" + id, { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getAlbumPhotos(id) {

        let accessToken = localStorage.getItem('access_token');
        let headers = new Headers();

        headers.append('Authorization', 'Bearer ' + accessToken);
        headers.append('Content-Type', 'application/json');

        return this.http.get(this.apiUrl + "/api/AlbumPhotoes/GetAlbumPhotos/" + id, { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    putAlbum(obj) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('access_token'));
        var body = JSON.stringify(obj);
        return this.http.put
            (this.apiUrl + '/api/albums/PutAlbum/' + obj.albumId, body, { headers });

    }

    deleteAlbum(id) {

        let accessToken = localStorage.getItem('access_token');
        let headers = new Headers();

        headers.append('Authorization', 'Bearer ' + accessToken);
        headers.append('Content-Type', 'application/json');

        return this.http.delete(this.apiUrl + "/api/albums/DeleteAlbum/" + id, { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    postNotice(formObj) {

        let accessToken = localStorage.getItem('access_token');

        let requestHeaders = new Headers();
        requestHeaders.append('Content-Type', 'application/json');
        requestHeaders.append('Authorization', 'Bearer ' + accessToken);

        let options = new RequestOptions({ headers: requestHeaders });
        let data = JSON.stringify(formObj);

        return this.http.post(this.apiUrl + '/api/Notices/PostNotice', data, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    postAlbum(formObj) {

        let accessToken = localStorage.getItem('access_token');

        let requestHeaders = new Headers();
        requestHeaders.append('Content-Type', 'application/json');
        requestHeaders.append('Authorization', 'Bearer ' + accessToken);

        let options = new RequestOptions({ headers: requestHeaders });
        let data = JSON.stringify(formObj);

        return this.http.post(this.apiUrl + '/api/albums/PostAlbum', data, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    postAlbumPhoto(formObj) {

        let accessToken = localStorage.getItem('access_token');

        let requestHeaders = new Headers();
        requestHeaders.append('Content-Type', 'application/json');
        requestHeaders.append('Authorization', 'Bearer ' + accessToken);

        let options = new RequestOptions({ headers: requestHeaders });
        let data = JSON.stringify(formObj);

        return this.http.post(this.apiUrl + '/api/AlbumPhotoes/PostAlbumPhoto', data, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    deleteAlbumPhoto(id) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('access_token'));

        return this.http.delete
            (this.apiUrl + '/api/AlbumPhotoes/DeleteAlbumPhoto/' + id, { headers });
    }

    deleteNotice(id) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('access_token'));

        return this.http.delete
            (this.apiUrl + '/api/Notices/DeleteNotice/' + id, { headers });
    }

    updateNotice(obj) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('access_token'));
        var body = JSON.stringify(obj);
        return this.http.put
            (this.apiUrl + '/api/Notices/PutNotice/' + obj.noticeId, body, { headers });

    }

    uploadPhoto(file) {

        var fileNameArray = file.name.split('.');

        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('access_token'));

        let formData: FormData = new FormData();
        formData.append('filename', file, file.name);
        formData.append('name', 'file');
        // formData.append('Content-Type', 'image/jpeg');
        formData.append('type', fileNameArray[0]);

        return this.http.post(this.apiUrl + '/api/admissions/UploadFile', formData, {
            headers: headers
        });
    }

    deleteStudent(id) {

        let accessToken = localStorage.getItem('access_token');
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + accessToken);

        return this.http.delete(this.apiUrl + "/api/Admissions/" + id, { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    makeFileRequest() {

        return new Promise((resolve, reject) => {

            var xhr = new XMLHttpRequest();
            let accessToken = localStorage.getItem('access_token')
            accessToken = "Bearer " + accessToken;

            xhr.open("GET", this.apiUrl + '/api/admissions/GetAdmissionExcel');
            xhr.setRequestHeader("Authorization", `${accessToken}`);
            xhr.setRequestHeader("Content-Type", 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {

                        resolve(xhr.response);
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.send();
        });
    }


    getAdmissionByEmail(email) {

        return this.http.get(this.apiUrl + "/api/admissions/GetAdmissionByEmail?email=" + email)
            .map(this.extractData)
            .catch(this.handleError);
    }


    private extractData(res: Response) {
        if (res.text()) {

            let body = res.json();
            return body;
        }
    }

    private statusResponse(res: Response) {

        return res;
    }

    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

    rejectAdmission(uid, status, statusnote, token) {

        let body = 'UserId=' + uid + '&Status=' + status + '&StatusNote=' + statusnote;

        let requestHeaders = new Headers();
        requestHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
        requestHeaders.append('Authorization', 'Bearer ' + token);

        return this.http.post(this.apiUrl + '/api/Admissions/UpdateStatus',
            body, { headers: requestHeaders })
            .map(this.statusResponse)
            .catch(this.handleError);
    }

    approveAdmission(uid, status, statusnote, token) {

        let body = 'UserId=' + uid + '&Status=' + status + '&StatusNote=' + statusnote;

        let requestHeaders = new Headers();
        requestHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
        requestHeaders.append('Authorization', 'Bearer ' + token);

        return this.http.post(this.apiUrl + '/api/Admissions/UpdateStatus',
            body, { headers: requestHeaders })
            .map(this.statusResponse)
            .catch(this.handleError);
    }

    markIncompleteAdmission(uid, status, statusnote, token) {

        let body = 'UserId=' + uid + '&Status=' + status + '&StatusNote=' + statusnote;

        let requestHeaders = new Headers();
        requestHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
        requestHeaders.append('Authorization', 'Bearer ' + token);

        return this.http.post(this.apiUrl + '/api/Admissions/UpdateStatus',
            body, { headers: requestHeaders })
            .map(this.statusResponse)
            .catch(this.handleError);
    }

    getFamilyIncome(familyIncomeBlock) {

        var incomes = ['Below Rs.50,000 ', 'Rs.100001 to 200000', 'Rs.200001 to 300000', 'Rs.300001 to 400000', 'Rs.400001 to 500000', 'Rs.500001 and above'];

        return incomes[familyIncomeBlock - 1];
    }

    getMonthName(monthNumber) {

        var monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'];

        return monthNames[monthNumber - 1];
    }

    getHumanReadableDate(dateReceived) {

        var dateArray = dateReceived.split("-");
        var day = dateArray[2].split("T");
        return dateArray[0] + "/" + this.getMonthName(dateArray[1]) + "/" + day[0];
    }
}