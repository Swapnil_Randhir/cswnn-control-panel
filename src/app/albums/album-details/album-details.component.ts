import { Component, OnInit, ViewChild} from '@angular/core';
import { Router, ActivatedRoute, Params, RouterModule } from '@angular/router';
import { AdmissionService } from '../../app.service';
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.css']
})
export class AlbumDetailsComponent implements OnInit {

  @ViewChild('cropper', undefined)
  public cropper: ImageCropperComponent; cropperSettings: CropperSettings; bounds: Bounds;
  data;

  selectedPhoto; photoResp; albumObj; noPhotoinAlbum = false;
  photos; albumPhotoObj;

  constructor(private _admissionService: AdmissionService, private router: Router) {

    this.data = {};
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 533;
    this.cropperSettings.height = 320;
    this.cropperSettings.croppedWidth = 533;
    this.cropperSettings.croppedHeight = 320;
    this.cropperSettings.canvasWidth = 400;
    this.cropperSettings.canvasHeight = 400;
    this.cropperSettings.rounded = false;
    this.cropperSettings.keepAspect = false;
    this.cropperSettings.noFileInput = true;

    this.bounds = new Bounds();
    this.bounds.left = 0;
    this.bounds.right = 0;
    this.bounds.top = 0;
    this.bounds.bottom = 0;
   }

  ngOnInit() {

    this.albumObj = new Album();
  }

  addAlbum(obj) {

    this._admissionService.postAlbum(obj).subscribe(

      data => {

        this.albumObj.albumId = data.albumId;

        alert("Album Created Successfully");

        this.postAlbumPhotos(this.albumObj);
        this.getAlbumPhotos(this.albumObj.albumId);
      }
    );
  }

  getAlbumPhotos(albumId) {

    this._admissionService.getAlbumPhotos(albumId).subscribe(

      data => {

        this.albumPhotoObj = new AlbumPhoto();
        this.albumPhotoObj.albumId = this.albumObj.albumId;

        if (data.length === 0) {

          this.noPhotoinAlbum = true;
        }

        else {

          this.photos = data;
        }
      }
    );
  }

  postAlbumPhotos(obj) {
    
    this._admissionService.postAlbumPhoto(obj).subscribe(
      data => {

        this.getAlbumPhotos(this.albumObj.albumId);
      }
    );
  }

  close() {
    
    this.router.navigate(['/albums']);
  }

  selectPhoto(event) {

    let eventObj = event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    this.selectedPhoto = files[0];

    if (this.selectedPhoto.size >= 3 * 1024 * 1024) {

      alert('Please select image of size less than 3 MB');
    }
    else {

      this.uploadPhoto();
    }
  }

  uploadPhoto() {

    this._admissionService.uploadPhoto(this.selectedPhoto).subscribe(
      data => {

        this.photoResp = data;
        const body = this.photoResp._body;
        this.photoResp = body;

        this.albumObj.imageUrl = this.photoResp;
      }
    );
  }

  getSelectedPhoto(event) {

    let eventObj = event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    this.selectedPhoto = files[0];

    // var image: any = new Image();
    // var myReader: FileReader = new FileReader();
    // var that = this;
    // myReader.onloadend = function (loadEvent: any) {

    //   image.src = loadEvent.target.result;
    //   that.cropper.setImage(image);
    // }

    // myReader.readAsDataURL(this.selectedPhoto);

    if (this.selectedPhoto.size >= 3 * 1024 * 1024) {

      alert('Please select image of size less than 3 MB');
    }
    else {

      this.addPhotoToAlbum();
    }
  }

  addPhotoToAlbum() {

    this._admissionService.uploadPhoto(this.selectedPhoto).subscribe(
      data => {

        this.photoResp = data;
        const body = this.photoResp._body;
        this.photoResp = body;
        this.albumPhotoObj.imageUrl = this.photoResp;
        // this.postAlbumPhotos(this.photoResp);
      }
    );
  }

}

export class Album {

  albumId: any;
  title: any;
  imageUrl: any;
  isPublished: any;

  constructor() { }
}

export class AlbumPhoto {

  albumId: any;
  title: any;
  imageUrl: any;
  isPublished: any;

  constructor() { }
}
