import { Component, OnInit } from '@angular/core';
import { AdmissionService } from './../app.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

  public albums = Array<Album>();
  public photos = Array<Photo>();
  openAlbum = false; showAlbums = true;
  albumDetails; selectedPhoto; photoResp; albumPhotos; albumId;

  constructor(private _admissionService: AdmissionService) {
  }

  ngOnInit() {

    this.getAlbums();
  }

  getAlbums() {

    this._admissionService.getAllAlbums().subscribe(

      data => {

        this.albums = data;
        this.albums = this.albums.reverse();
      }
    );
  }

  updateAlbum(album) {

    this._admissionService.putAlbum(album).subscribe(

      data => {
        alert("Saved Successfully");
      }
    );
  }

  closeAlbum() {

    this.openAlbum = false;
    this.showAlbums = true;
    this.getAlbums();
  }

  openSelectedAlbum(album) {

    this.albumId = album;

    this.openAlbum = true;
    this.showAlbums = false;

    this._admissionService.getAlbum(album).subscribe(
      data => {

        this.albumDetails = data;

        this.getAlbumPhotos(album);
      }
    );
  }

  getAlbumPhotos(id) {

    this._admissionService.getAlbumPhotos(id).subscribe(

      data => {

        this.albumPhotos = data;
      }
    );
  }

  postAlbumPhotos(obj) {

    this._admissionService.postAlbumPhoto(obj).subscribe(
      data => {

        this.getAlbumPhotos(this.albumDetails.albumId);
      }
    );
  }

  selectPhoto(event) {

    let eventObj = event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    this.selectedPhoto = files[0];

    if (this.selectedPhoto.size >= 3 * 1024 * 1024) {

      alert('Please select image of size less than 3 MB');
    }
    else {

      this.uploadPhoto();
    }
  }

  uploadPhoto() {

    this._admissionService.uploadPhoto(this.selectedPhoto).subscribe(
      data => {

        this.photoResp = data;
        var body = this.photoResp._body;
        this.photoResp = body;
        this.albumDetails.imageUrl = this.photoResp;
      }
    );
  }

  selectImageToUpolad(event) {

    let eventObj = event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    this.selectedPhoto = files[0];

    if (this.selectedPhoto.size >= 3 * 1024 * 1024) {

      alert('Please select image of size less than 3 MB');
    }
    else {

      this.addPhotoToAlbum();
    }
  }

  addPhotoToAlbum() {

    this._admissionService.uploadPhoto(this.selectedPhoto).subscribe(
      data => {

        this.photoResp = data;
        const body = this.photoResp._body;
        this.photoResp = body;

        this.postAlbumPhotos(
          {
            albumId: this.albumDetails.albumId,
            imageUrl: this.photoResp
          }
        );
      }
    );
  }


  removeAlbumPhoto(photo) {

    if (confirm("Are you sure you want to remove this photo?")) {

      this._admissionService.deleteAlbumPhoto(photo).subscribe(
        data => {

          this.getAlbumPhotos(this.albumId);
        }
      );
    }
  }

  deleteAlbum(album) {

    if (confirm("Are you sure you want to delete this album?")) {

      this._admissionService.deleteAlbum(album).subscribe(
        data => {

          this.getAlbums();
        }
      );
    }
  }

  publishAlbum(album) {

    album.isPublished = true;

    this.updateAlbum(album);
  }

  unPublishAlbum(album) {

    album.isPublished = false;

    this.updateAlbum(album);
  }

}

export class Album {

  title: string;
  photos: Photo[];

  constructor() {

    this.photos = [];
  }
}

export class Photo {

  url: string;

  constructor() { }
}